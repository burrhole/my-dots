(setq user-full-name "Alice Salem"
      user-mail-address "alicesalem93@gmail.com")

(setq doom-font (font-spec :family "monospace" :size 14 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "sans" :size 15))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(setq doom-theme 'doom-dracula)

(setq org-directory "~/Documents/"
      org-hide-emphasis-markers t)

(setq display-line-numbers-type 'relative)
(setq-default major-mode 'text-mode)

(use-package treemacs
  :init
  (setq treemacs-indentation-string (propertize " | " 'face 'font-lock-comment-face)
        treemacs-indentation 1))

(use-package simple-mpc
  :commands (simple-mpc)
  :config
  (evil-set-initial-state 'simple-mpc-mode 'emacs))

(map! :leader "o M" #'simple-mpc)
(map! :leader "t p" #'simple-mpc-toggle)
(add-hook! 'doom-escape-hook
  (defun forward-escape-key ()
    (when (derived-mode-p 'vterm-mode)
      (vterm-send-C-c))))

(defadvice! fix-abort-if-file-too-large-a (orig-fn size op-type filename &optional offer-raw)
:around #'abort-if-file-too-large
(unless (string-match-p "\\.pdf\\'" filename)
  (funcall orig-fn size op-type filename offer-raw)))

(setq confirm-kill-emacs nil)
